function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    if((obj instanceof Object === false || Array.isArray(obj))|| JSON.stringify(obj) === '{}'){
      return 'invalid input or empty object'
    }
    let array = []
    for(let element in obj){
      let tempArray = []
      tempArray.push(element,obj[element])
      array.push(tempArray)
    }
    return array
}
module.exports = pairs
