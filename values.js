function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    if((obj instanceof Object === false || Array.isArray(obj))|| JSON.stringify(obj) === '{}'){
      return 'invalid input or empty object'
    }
    let arrayOfValues = []
    for(let element in obj){
      arrayOfValues.push(obj[element])
    }
    return arrayOfValues
}
module.exports = values
