function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
    if((obj instanceof Object === false || Array.isArray(obj))|| JSON.stringify(obj) === '{}'){
      return 'invalid input or empty object'
    }
    else if((defaultProps instanceof Object === false || Array.isArray(defaultProps))|| JSON.stringify(defaultProps) === '{}'){
      return 'invalid input or empty object'
    }
    for(let element in defaultProps){
      if(!obj.hasOwnProperty(element)){
        obj[element] = defaultProps[element]
      }
    }
    return obj
}
module.exports = defaults
