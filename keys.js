function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
    if((obj instanceof Object === false || Array.isArray(obj))|| JSON.stringify(obj) === '{}'){
      return 'invalid input or empty object'
    }
    let arrayOfKeys = []
    for(let element in obj){
      arrayOfKeys.push(element)
    }
    return arrayOfKeys
}
module.exports = keys
