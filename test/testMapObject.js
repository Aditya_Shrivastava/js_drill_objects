const mapObject = require('../mapObject.js')
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions
function transform_value(value){
  if(typeof value === 'string'){
    return value.toUpperCase()
  }
  else if(typeof value === 'number'){
    return value*value
  }
  else{
    return value
  }
}

let returnedobj = mapObject(testObject,transform_value)
console.log(returnedobj)
console.log(mapObject({},transform_value))
console.log(mapObject([2,3,4,5], transform_value))
console.log(mapObject(testObject,'transform_value'))
