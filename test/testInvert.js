const invert = require('../invert.js')
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions
let invertedObj = invert(testObject)
console.log(invertedObj)
console.log(invert({}))
console.log(invert('testObject'))
console.log(invert([[2,3],[5,6]]))
