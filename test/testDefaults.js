const defaults = require('../defaults.js')
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions
const defaultObj = {contact: 123456789, address: 'abc', location: 'DC universe'}
let newObj = defaults(testObject,defaultObj)
console.log(newObj)
console.log(defaults(testObject,{}))
console.log(defaults({},defaultObj))
console.log(defaults(testObject,'defaultObj'))
