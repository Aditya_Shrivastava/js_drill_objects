function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    if((obj instanceof Object === false || Array.isArray(obj))|| JSON.stringify(obj) === '{}'){
      return 'invalid input or empty object'
    }
    else if(cb instanceof Function === false){
      return 'callback argument is not a function'
    }
    for(let element in obj){
      let temp = cb(obj[element])
      obj[element] = temp
    }
    return obj
}
module.exports = mapObject
